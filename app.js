const express = require("express");
// const mongoose = require("mongoose");
const cors = require("cors");
// const bodyParser = require("body-parser");
require("dotenv").config();

const userRoute = require("./routes/user");
const capilRoute = require("./routes/capil");

const dbconfig = process.env.MONGO_PATH;
const app = express();
app.use(express.json());
app.use(cors());
const PORT = process.env.PORT || "3000";

const path = require("path");
app.use(express.static(path.join(__dirname, "build")));
app.get("/", function (req, res) {
	res.sendFile(path.join(__dirname, "build", "index.html"));
});

// app.get("/", (req, res) => res.send("Hello Express !"));

app.use("/api/user", userRoute);
app.use("/api/capil", capilRoute);

//start the server
// const db = mongoose.connect(
//   dbconfig,
//   { useNewUrlParser: true, useUnifiedTopology: true },
//   () => {
//     app.listen(PORT, () =>
//       console.log("Db Connected & Node Server Running on Port : " + PORT)
//     );
//   }
// );

app.listen(PORT, () => console.log("Node Server Running on Port : " + PORT));
