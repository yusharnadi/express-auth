const express = require("express");
const fetch = require("node-fetch");
const router = express.Router();

// GET PENDUDUK BY JENIS KELAMIN
router.get("/penduduk_jk", (req, res) => {
	fetch("http://207.148.118.33:7300/ws/pddk_jk.json")
		.then((response) => response.json())
		.then((response) => {
			let dataJk = response.data;
			return res.status(200).json({ status: "ok", dataJk });
		})
		.catch((err) => {
			console.log(err);
			return res.status(500).json({ status: "error", message: err });
		});
});

// GET PENDUDUK BY AGAMA
router.get("/penduduk_agama", (req, res) => {
	fetch("http://207.148.118.33:7300/ws/pddk_agama.json")
		.then((response) => response.json())
		.then((response) => {
			res.status(200).json({ status: "ok", response });
		})
		.catch((err) => {
			console.log(err);
			return res.status(500).json({ status: "error", message: err });
		});
});

// GET PENDUDUK BY KECAMATAN
router.get("/penduduk_kecamatan", (req, res) => {
	fetch("http://207.148.118.33:7300/ws/pddk_kec.json")
		.then((response) => response.json())
		.then((response) => {
			res.status(200).json({ status: "ok", response });
		})
		.catch((err) => {
			console.log(err);
			return res.status(500).json({ status: "error", message: err });
		});
});

// PENDUDUK BY PEKERJAAN
router.get("/penduduk_pekerjaan", (req, res) => {
	fetch("http://207.148.118.33:7300/ws/pddk_pekerjaan.json")
		.then((response) => response.json())
		.then((response) => {
			// let data = response.PENDUDUK_PEKERJAAN;
			res.status(200).json({ status: "ok", response });
		})
		.catch((err) => {
			console.log(err);
			return res.status(500).json({ status: "error", message: err });
		});
});

module.exports = router;
