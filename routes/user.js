const express = require("express");
const User = require("../models/User");
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
var router = express.Router();
const Joi = require("@hapi/joi");

router.get("/", async (req, res) => {
  try {
    const users = await User.find();
    return res.status(200).json(users);
  } catch (error) {
    return res.status(400).json({ message: error.message });
  }
});

router.get("/:id", (req, res) => {
  // if (!mongoose.Types.ObjectId.isValid(req.params.id))
  //   return res.status(404).json({ message: "invalid ID Type" });
  User.findById(req.params.id)
    .then(data => {
      res.status(200).json(data);
    })
    .catch(err => {
      res.status(500).json({ status: "Error", message: "User not Found !" });
    });
});

router.post("/register", async (req, res) => {
  const { error } = validationRules(req.body);
  if (error) {
    return res.status(422).json(error.details[0].message);
  }

  const salt = await bcrypt.genSalt(10);
  const { name, username, email, password } = req.body;
  const PassHash = await bcrypt.hash(password, salt);

  const newUser = new User({
    username: username,
    email: email,
    name: name,
    password: PassHash
  });

  // cek user exist
  const userExist = await User.findOne({ username: username, email: email });
  if (userExist) {
    return res.status(200).json({ message: "User Exist !" });
  }

  try {
    const data = await newUser.save();
    return res
      .status(200)
      .json({ message: "success", result: { _id: data._id } });
  } catch (error) {
    return res.status(500).json({ message: error });
  }
});

router.put("/:id", async (req, res) => {
  //Cek User Exist
  if (!mongoose.Types.ObjectId.isValid(req.params.id))
    return res.status(404).json({ message: "invalid ID Type" });
  const userExist = await User.findById(req.params.id);
  if (!userExist) {
    return res
      .status(404)
      .json({ status: "Error", message: "User not found !" });
  }

  if (req.body.password) {
    const salt = await bcrypt.genSalt(10);
    req.body.password = await bcrypt.hash(req.body.password, salt);
  }

  try {
    const updated = await User.update(
      { _id: req.params.id },
      { $set: req.body }
    );
    return res.status(200).json({ status: "success", id: userExist._id });
  } catch (error) {
    return res.status(500).json({ status: "Error", message: error });
  }
});

router.delete("/:id", async (req, res) => {
  //Cek User Exist
  if (!mongoose.Types.ObjectId.isValid(req.params.id))
    return res.status(404).json({ message: "invalid ID Type" });
  const userExist = await User.findById(req.params.id);
  if (!userExist) {
    return res
      .status(404)
      .json({ status: "Error", message: "User not found !" });
  }

  try {
    await User.deleteOne({ _id: req.params.id });
    return res.status(200).json({ status: "success", _id: userExist._id });
  } catch (error) {
    return res.status(500).json({ status: "Error", message: error });
  }
});

router.post("/cek", async (req, res) => {
  const cek = await bcrypt.compare(
    "12345",
    "$2a$10$nvYQpXgixcAWxKpXulCBo.5rSJeJe415T8OanHp9WOSJpF6wm4ZfC"
  );
  console.log(req.body);
  return res.send("cek");
});

router.get("/cek/andi", async (req, res) => {
  const response = await fetch("http://207.148.118.33:7300/ws/pddk_jk.json");
  if (!response) {
    return res.status(500).json({ err: "error" });
  }
  const data = response.json();
  res.status(200).json({ uye: "uiii", data });
});
// VALIDATION RULES SCHEMA
const validationRules = inputObj => {
  // Register Schema Validation
  const registerSchema = Joi.object().keys({
    _id: Joi.string(),
    name: Joi.string()
      .min(4)
      .required(),
    username: Joi.string()
      .min(3)
      .required(),
    email: Joi.string()
      .min(3)
      .required()
      .email(),
    password: Joi.string()
      .min(6)
      .required()
  });

  return registerSchema.validate(inputObj);
};

module.exports = router;
